#' Title
#'
#' @param model
#'
#' @return
#' @export
#'
#' @examples
pssample <- function(model){
  if(!is(model,"crowd")) stop("Argument not valid")
  y_list = unlist(model$y)
  nn = model$nn
  NN = model$NN
  N = sum(NN)
  n = sum(nn)

    sample_index <- sample(N, n)
    y_sample <- y_list[sample_index]

    index <- list(seq(NN[1]), seq(NN[1]+1, NN[1]+NN[2]), seq(NN[1]+NN[2]+1, NN[1]+NN[2]+NN[3]), seq(NN[1]+NN[2]+NN[3]+1, NN[1]+NN[2]+NN[3]+NN[4]))
    c <- c()
    for(i in 1:4){
      c <- c(c, sum(index[[i]] %in% sample_index))
    }
    c
    ps <- c/nn
    # HT estimator and variance
    Htest<- sum(mean(y_sample)*ps)/sum(ps)
    var_sr <- var(y_sample)
    var_Htest<-(N^-2)*sum(ps*NN*var_sr)



  out <- list(method = "Post sampling",
              Htest = mean(Htest),
              var_Htest = mean(var_Htest),
              y_sample = y_sample,
              model = model
  )
  class(out) <- "crowdsample"
  return(out)
}
