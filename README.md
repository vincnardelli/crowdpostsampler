crowdpostsampler
================

The goal of crowdpostsampler is to ...

Example
-------

This is a basic example which shows you how to solve a common problem:

``` r

my_crowd <- gencrowd(NN = c(800, 60, 60, 80),
   nn = c(20, 10, 10, 20),
          lambda = 0.7)
print(my_crowd)
#> 
#>  Crowdsourcing spatial simulation
#>  Input
#>  NN  800 60 60 80
#>  nn  20 10 10 20
#>  lambda 0.7 
#> 
#> Moran I statistic       Expectation          Variance 
#>      2.801937e-02     -1.001001e-03      1.702066e-05
plot(my_crowd)
```

![](man/figure/README-my_crowd-1.png)
